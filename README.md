`dotimes*` is a straighforward extension of (cl-)dotimes.

It is more flexible than dotimes, but simpler than cl-loop.

`dotimes*` allows a starting value and (possibly negative)
step size to be given.

Like cl-dotimes, but unlike elisp dotimes,
`dotimes*` allows early exit via `cl-return`.



