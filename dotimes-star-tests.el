;;; dotimes-star.el --- extension for dotimes    -*- lexical-binding: t -*-

;; (derived work)
;; Copyright (C) 2023 Paul Horton (concat [112 97 117 108 104 64 105 115 99 98 46 111 114 103])
;;
;; (base work, elisp dotimes in emacs 28.1 subr.el)
;; Copyright (C) 1985-1986, 1992, 1994-1995, 1999-2023 Free Software Foundation, Inc.

;; License: GPLv3

;; Maintainer: Paul Horton
;; Created: 20230613
;; Updated: 20231007
;; Version: 0.0.1
;; Keywords: loop, dotimes

;;; Commentary:

;; regression tests for dotimes*

;;; Change Log:

;;; Code:


(ert-deftest dotimes*/no-iteration ()
    "A case which should iterate zero times."
  (should
   (=
    3
    (let1 x 3
      (dotimes* (i 4 4 -1)
        (cl-incf x i)
        )
      x
      )))
  (should; control test which should iterate once.
   (=
    7
    (let1 x 3
      (dotimes* (i 4 3 -1)
        (cl-incf x i)
        )
      x
      ))))



(ert-deftest dotimes*/returns-nil-by-default ()
  "Return nil by default"
  (should
   (not
    (dotimes* (_ 1 10 2);  Return not stipulated.  Should default to nil.
      (ignore)
      ))))


(ert-deftest dotimes*/early-exit ()
  (should
   (= 17
      (dotimes* (i 10 100 1 'retval)
        (when (= i 17) (cl-return i))
        ))))


(ert-deftest dotimes*/loop1 ()
  "Tries out one simple loop"
  (should
   (= 45
      (let ((sum 0))
        (dotimes* (i 0 10 1 sum)
          (setq sum (+ sum i))
          ))
      )
    ))


(ert-deftest dotimes*/loop1-down ()
  "Tries out simple, down counting loop"
  (should
   (equal
    '(6 4 2 0 -2 -4 -6)
    (let (ret-list)
      (dotimes* (i 6 -7 -2 (reverse ret-list))
        (push i ret-list)
        ))
      )
    ))


(ert-deftest dotimes*/return-constant ()
  "Returns constant"
  (should
   (= 7
      (dotimes* (i 0 10 1 7)
        (ignore i)
        ))
      )
    )


(ert-deftest dotimes*/return-early ()
  "Uses `cl-return' to return early.
finding the smallest number whose square is greater than 100"
  (should
   (= 11
      (dotimes* (i 1 100)
        (if (< 100 (* i i))
            (cl-return i)
          )))))

(ert-deftest dotimes*/return-early-with-retval-spec ()
  "Uses `cl-return' to return early.
finding the smallest number whose square is greater than 100,
when true, the cl-return value has precedence over the
RETURN value in spec."
  (should
   (= 11
      (dotimes* (i 1 100 1 'me)
        (if (< 100 (* i i))
            (cl-return i)
          )))))


(ert-deftest dotimes*/loop2 ()
  "Tries out simple nested loop"
  (should
   (=
    (cl-loop
     for i from 1 below 10
     sum
     (cl-loop
      for j from 20 above 3 by 2
      sum (* i j)
      ))
    (let ((sum 0))
      (dotimes* (i 1 10 1 sum)
        (dotimes* (j 20 3 -2)
          (setq sum (+ sum (* i j)))
          ))
      )
    )))


;;; dotimes-star-tests.el ends here
