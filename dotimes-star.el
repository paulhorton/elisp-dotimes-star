;;; dotimes-star.el --- Extension of dotimes  -*- lexical-binding: t -*-

;; (derived work)
;; Copyright (C) 2023 Paul Horton (concat [112 97 117 108 104 64 105 115 99 98 46 111 114 103])
;;
;; (base work, elisp dotimes in emacs 28.1 subr.el)
;; Copyright (C) 1985-1986, 1992, 1994-1995, 1999-2023 Free Software Foundation, Inc.

;; License: GPLv3

;; Maintainer: Paul Horton
;; Created: 20230613
;; Updated: 20231007
;; Version: 0.0.1
;; Keywords: loop, dotimes

;;; Commentary:

;; Extension of (cl-)dotimes to support user supplied
;; initial value and optionally step size (possibly negative).
;;
;; More flexible than dotimes and much simpler than cl-loop.
;;
;; Like, cl-dotimes (but unlike elisp dotimes),
;; dotimes* supports eary exit via `cl-return'
;;
;; The implementation is an extension of the dotimes code in subr.el of emacs 28.1,
;;
;;
;; Regarding the optional RESULT argument.
;; Both common lisp and elisp dotimes allow the user to optionally stipulate a return value RESULT.
;; But in emacs version 28 stipulating a return value has been deprecated.
;;
;; I think being able to stipulate a return value is convenient,
;; so dotimes* supports this.
;;
;; However there is a minor difference between dotimes and dotimes*.
;; dotimes allows the RESULT expression to access the loop variable, which is set up to
;; hold the final value the loop variable held.
;;
;; I am not aware of a use case for this "feature" and it has the disadvantage of
;; making it difficult implement the dotimes macro without generating unused variable
;; compiler errors.
;;
;; So dotimes* does not support this "can access loop variable final value" feature,
;; but otherwise does support stipulating a return value.
;;
;;
;;; Change Log:

;;; Code:

(require 'cl-lib)


(defmacro dotimes* (spec &rest body)
  "Loop a certain number of times.
Evaluate BODY with VAR bound to successive integers
running from START (inclusive) to LIMIT (exclusive).

Optional STEP should be a step size (defaulting to 1).
If STEP is negative than LIMIT should be less than START.

Finally if RESULT is given, evaluate it are return this value
(nil is return if RESULT is omitted).

To stipulate RESULT you must explicitly give a step size even
if it is 1.
for example:
  (dotimes (i 0 9 1 x) ...)

Like `cl-dotimes', the BODY is wrapped in a nil cl-block
so `cl-return' can be used for early exit.

The return result is actually: (or cl-return-val RESULT)
So a true value returned via cl-return has precedence over RESULT in SPEC.

\(fn (VAR START LIMIT [STEP [RESULT]]) BODY...)"
  (declare
   (indent 1)
   (debug ((symbolp form form &optional form form) body))
   )
  (or body (warn "dotimes* empty body"))
  (let (
        (var          (nth 0 spec))
        (start-exp    (nth 1 spec))
        (limit-exp    (nth 2 spec))
        (step-exp (or (nth 3 spec) 1))
        (limit (make-symbol "limit"))
        (step  (make-symbol "step"))
        (count (make-symbol "count"))
        )
    (cl-assert (symbolp var) nil (format "Expected spec to start with symbol but got %S" var))
    (cl-assert  limit-exp    t   (format "Expected spec to have at least 3 non-nil elements"))
    `(let (;; Evaluate expressions passed to dotimes* macro only once.
           (,limit ,limit-exp)
           (,step  ,step-exp)
           (,count ,start-exp)
           )
       (or
        (catch '--cl-block-nil--
          (if (< 0 ,step)
              (while (< ,count ,limit)
                (let ((,var ,count)) ,@body)
                (setq ,count (+ ,step ,count))
                )
            (while (> ,count ,limit)
              (let ((,var ,count)) ,@body)
              (setq ,count (+ ,step ,count))
              )
            ))
        ,@(cddddr spec); Note cddddr conveniently returns nil when spec is too short.
        ))))



(provide 'dotimes-star)


;;; dotimes-star.el ends here
